package com.ejia.mogo.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MogoReciverForReboot extends BroadcastReceiver
{
  public void onReceive(Context context, Intent intent)
  {
    String action = intent.getAction();
    Log.d("ACTION_BOOT_COMPLETED", "ACTION_BOOT_COMPLETED");
    if (action.equals("android.intent.action.BOOT_COMPLETED"))
    {
      Intent i = new Intent("com.example.notificationtest.MyService");
      i.putExtra("reboot", true);
      context.startService(i);
    }
  }
}