package com.ejia.mogo.notification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.net.ParseException;

public class MogoNotificationData
{
  public static final int TYPE_ADD = 0;
  public static final int TYPE_REMOVE = 1;
  public String time;
  public long gmtTime = 0L;
  public int id;
  public int type;
  public int tag;
  public String title1;
  public String title2;
  public String content;
  public int group;

  public long getTime() throws java.text.ParseException
  {
    if (this.gmtTime != 0L) {
      return this.gmtTime;
    }
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    try
    {
      Date date = format.parse("2013-12-11 " + this.time);
      Calendar c = Calendar.getInstance();
      int year = c.get(1);
      int month = c.get(2);
      int day = c.get(5);
      int hour = Integer.parseInt(this.time.split(":")[0]);
      Calendar c2 = Calendar.getInstance();
      c2.setTime(date);

      Calendar cal = Calendar.getInstance();
      cal.set(year, month, day, hour, c2.get(12), 
        c2.get(13));
      return cal.getTime().getTime();
    }catch(java.text.ParseException e){
    	
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }return -1L;
  }

  public MogoNotificationData(String time, int type, int tag, String title1, String title2, String content, long gmtTime, int id)
  {
    this(time, type, tag, title1, title2, content, id);
    this.gmtTime = gmtTime;
  }

  public MogoNotificationData(String time, int type, int tag, String title1, String title2, String content, int id)
  {
    this.time = time;
    this.type = type;
    this.tag = tag;
    this.title1 = title1;
    this.title2 = title2;
    this.content = content;
    this.id = id;
  }
}