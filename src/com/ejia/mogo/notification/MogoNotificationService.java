package com.ejia.mogo.notification;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class MogoNotificationService extends IntentService
{
  public static final String MYSERVICE_INTENT = "com.example.notificationtest.MyService";

  public MogoNotificationService()
  {
    super("MyService");
  }

  protected void onHandleIntent(Intent intent)
  {
    Log.d("MainActivity", "onHandleIntent");
    boolean isFromReboot = intent.getBooleanExtra("reboot", false);
    if (isFromReboot) {
      MogoNotificationManager.updateConfig(this, -1, 
        1);
      MogoNotificationManager.getOneNotificationToShow(this);
    }
    else {
      String title1 = intent.getStringExtra("title1");
      String title2 = intent.getStringExtra("title2");
      String content = intent.getStringExtra("content");
      int type = intent.getIntExtra("type", 0);
      int tag = intent.getIntExtra("tag", 0);
      int id = intent.getIntExtra("id", 0);

      MogoNotificationManager.showNotification(this, title1, title2, content, type, tag, id);
    }
  }
}