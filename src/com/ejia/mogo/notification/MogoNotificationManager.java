package com.ejia.mogo.notification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

public class MogoNotificationManager
{
  static String DEBUG_TAB = "MogoNotificationManager";
  public static final String FULL_ENERGY_CONFIG_TAG = "fullEnergyTime";
  public static final String CONFIG_FIELE_NAME = "notification_cofig";
  public static final String DAILY_CONFIG_ID_LIST = "DAILY_CONFIG_ID_LIST";
  static ArrayList<MogoNotificationData> mDataList = new ArrayList();

  static { mDataList.add(new MogoNotificationData("12:00:00", 
      0, 1, "圣泉祝福活动开始啦-暗黑战神", 
      "圣泉祝福活动开始啦-暗黑战神", "赶快来接受圣泉的祝福吧，立即回复60点体力！", 1));
    mDataList.add(new MogoNotificationData("18:00:00", 
      0, 2, "圣泉祝福活动开始啦-暗黑战神", 
      "圣泉祝福活动开始啦-暗黑战神", "赶快来接受圣泉的祝福吧，立即回复60点体力！", 1));

    mDataList.add(new MogoNotificationData("20:30:00", 
      0, 4, "魔龙逆袭开始啦-暗黑战神", 
      "魔龙逆袭开始啦-暗黑战神", "伟大的主人，魔龙拉东来袭，只有您才能够拯救圣域！", 2));

    mDataList.add(new MogoNotificationData("21:00:00", 
      0, 8, "守护女神多人副本开始啦-暗黑战神", 
      "守护女神多人副本开始啦-暗黑战神", "守护女神多人副本开始啦-暗黑战神,赶快和小伙伴们一起保卫女神，免受怪物的侵扰！", 
      4));
  }

  public static void stopNotification(Context context)
  {
    AlarmManager am = (AlarmManager)context
      .getSystemService("alarm");
    Intent intent = new Intent("com.example.notificationtest.MyService");
    PendingIntent sender = PendingIntent.getService(context, 0, intent, 
      268435456);
    am.cancel(sender);
  }

  public static void addEnergyFullNotification(Context context, long time) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);

    Log.d("addEnergyFullNotification", "addEnergyFullNotification");
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putLong("fullEnergyTime", 
      time + System.currentTimeMillis());
    editor.commit();
  }

  private static MogoNotificationData getEnergyFullNotification(Context context)
  {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);

    long time = sharedPreferences.getLong("fullEnergyTime", 0L);
    long d = time - System.currentTimeMillis();
    if (d <= 0L) {
      return null;
    }
    MogoNotificationData notificationData = new MogoNotificationData("", 
      0, 64, "亲爱的主人，告诉您一个秘密！", 
      "亲爱的主人，告诉您一个秘密！", "您的体力回复满了哟，再不上线就要浪费啦！", time, 5);
    return notificationData;
  }

  public static void toSetupNotification(String gameObject)
  {
    UnityPlayer.UnitySendMessage(gameObject, "OnSetupNotification", 
      "OnSetupNotification");
  }

  public static void onSetupDone(Context context, String data) {
    Log.d(DEBUG_TAB, "onSetupDone");
    saveConfig(data);

    MogoNotificationData notificationData = getNotification(context);
    toShowNotification(context, notificationData);
  }

  private static void toShowNotification(Context context, MogoNotificationData notificationData)
  {
    if (notificationData == null) {
      Log.e("haha", "there is notification can be sended");
      return;
    }

    Log.d(DEBUG_TAB, "toShowNotification");

    AlarmManager am = (AlarmManager)context
      .getSystemService("alarm");
    Intent intent = new Intent("com.example.notificationtest.MyService");

    intent.putExtra("title1", notificationData.title1);
    intent.putExtra("title2", notificationData.title2);
    intent.putExtra("content", notificationData.content);
    intent.putExtra("type", notificationData.type);
    intent.putExtra("tag", notificationData.tag);
    intent.putExtra("id", notificationData.id);

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    PendingIntent sender = PendingIntent.getService(context, 0, intent, 
      268435456);
    long time;
	try {
		time = notificationData.getTime();
		am.set(0, time, sender);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
  }

  private static ArrayList<MogoNotificationData> getDailyNotificationList(Context context)
  {
    ArrayList dataList = new ArrayList();
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);
    String idListStr = sharedPreferences
      .getString("DAILY_CONFIG_ID_LIST", "");
    if (idListStr.equals(""))
      return dataList;
    String[] idList = idListStr.split("_");

    for (int i = 0; i < idList.length; i++)
    {
      String[] info = sharedPreferences.getString(idList[i], "").split(
        "_");
      MogoNotificationData data = new MogoNotificationData(idList[i], 
        0, 
        1 << Integer.parseInt(info[1]), info[2], info[3], info[4], 
        Integer.parseInt(info[0]));
      Log.d("haha2", 
        "time:" + idList[i] + ",tag:" + (
        1 << Integer.parseInt(info[1])) + ",title1:" + 
        info[2] + ",title2:" + info[3] + ",content:" + 
        info[4] + ",id:" + Integer.parseInt(info[0]));
      dataList.add(data);
    }
    return dataList;
  }

  private static MogoNotificationData getNotification(Context context)
  {
    long currentTime = System.currentTimeMillis();
    long minD = 9223372036854775807L;
    long maxD = -9223372036854775808L;
    int minIndex = -1;
    int maxIndex = -1;

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");

    ArrayList mDataListTemp = new ArrayList(
      getDailyNotificationList(context));

    MogoNotificationData fulleneryNotification = getEnergyFullNotification(context);
    if (fulleneryNotification != null) {
      mDataListTemp.add(fulleneryNotification);
    }

    Calendar c = Calendar.getInstance();
    c.set(5, c.get(5) + 1);
    c.set(11, 0);
    c.set(12, 1);
    c.set(13, 0);

    mDataListTemp.add(new MogoNotificationData("00:00:00", 
      1, -1, "", "", "", c.getTime()
      .getTime(), -1));

    for (int i = 0; i < mDataListTemp.size(); i++)
    {
      MogoNotificationData temp = (MogoNotificationData)mDataListTemp.get(i);
      long d;
	try {
		d = temp.getTime() - currentTime;
	      if ((d < 0L) && (d > maxD)) {
	        maxD = d;
	        maxIndex = i;
	      } else if ((d > 0L) && (d < minD) && (canSend(context, temp.tag))) {
	        minD = d;
	        minIndex = i;
	      }
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }

    if (minIndex != -1) {
      return (MogoNotificationData)mDataListTemp.get(minIndex);
    }

    return null;
  }

  private static boolean canSend(Context context, int tag)
  {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);
    if (tag == -1) {
      return true;
    }
    int historyTag = sharedPreferences.getInt("tag", 0);

    boolean hasSend = (historyTag & tag) != 0;
    return !hasSend;
  }

  private static void saveConfig(String data)
  {
  }

  public static void showNotification(Context context, String title1, String title2, String content, int type, int tag, int id)
  {
    Log.d(DEBUG_TAB, "showNotification");

    if (type == 0) {
      doShowNOtification(id, title1, title2, content, context);
    }

    updateConfig(context, tag, type);

    MogoNotificationData notificationData = getNotification(context);

    toShowNotification(context, notificationData);
  }

  public static void getOneNotificationToShow(Context context) {
    MogoNotificationData notificationData = getNotification(context);

    toShowNotification(context, notificationData);
  }

  public static void updateConfig(Context context, int tag, int type) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);

    SharedPreferences.Editor editor = sharedPreferences.edit();

    int oldvalue = sharedPreferences.getInt("tag", 0);

    if (type == 0) {
      int newValue = oldvalue | tag;

      editor.putInt("tag", newValue);
    } else if (type == 1)
    {
      int newValue = oldvalue & (tag ^ 0xFFFFFFFF);

      editor.putInt("tag", newValue);
    }

    editor.commit();
  }

  private static void doShowNOtification(int id, String title1, String title2, String content, Context context)
  {
    Notification mNotification = new Notification();

    mNotification.icon = 2130837581;
    mNotification.tickerText = title1;
    mNotification.when = System.currentTimeMillis();

    mNotification.defaults |= -1;

    mNotification.flags = 16;

    ComponentName componetName = new ComponentName(
      context.getApplicationInfo().packageName, 
      context.getApplicationInfo().packageName + ".MainActivity");

    Intent intent = new Intent();
    intent.setComponent(componetName);

    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, 
      intent, 0);
    mNotification.setLatestEventInfo(context, title2, content, 
      contentIntent);

    NotificationManager mNotificationManager = (NotificationManager)context
      .getSystemService("notification");

    mNotificationManager.notify(id, mNotification);
  }

  public static void updateNotification(String data)
  {
  }

  public static void RemoveAllDailyNotification(Context context)
  {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
      "notification_cofig", 0);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    String idListStr = sharedPreferences
      .getString("DAILY_CONFIG_ID_LIST", "");
    if (idListStr.equals(""))
      return;
    String[] idList = idListStr.split("_");
    for (int i = 0; i < idList.length; i++) {
      editor.remove(idList[i]);
    }
    editor.remove("DAILY_CONFIG_ID_LIST");
    editor.commit();
  }

  public static void setupDailyNotification(Context mainActivity, String time, String type, String tag, String title1, String title2, String content)
  {
    RemoveAllDailyNotification(mainActivity);

    SharedPreferences sharedPreferences = mainActivity
      .getSharedPreferences("notification_cofig", 0);

    Log.d("setupDailyNotification", "setupDailyNotification");
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putString("DAILY_CONFIG_ID_LIST", time);

    String[] timeList = time.split("_");
    String[] typeList = type.split("_");
    String[] tagList = tag.split("_");
    String[] title1List = title1.split("_");
    String[] title2List = title2.split("_");
    String[] contentList = content.split("_");

    for (int i = 0; i < timeList.length; i++) {
      editor.putString(timeList[i], typeList[i] + "_" + tagList[i] + "_" + 
        title1List[i] + "_" + title2List[i] + "_" + 
        contentList[i]);
    }

    editor.commit();
  }
}
