package com.ejia.mogo;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

import com.ejia.mogo.notification.MogoNotificationManager;
import com.ejia.mogo.utils.ILoginCallback;
import com.ejia.mogo.utils.MogoUpdateCallback;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;


public abstract class MogoMainActivityParent extends UnityPlayerActivity
  implements ILoginCallback
{
  protected Context mContext;
  protected Vibrator mVibrator;
  public static final String DEBUG_TAB = "MainActivity";
  protected String mGameobject;
  protected String mGoUpdateCallBack;
  protected MogoUpdateCallback mUpdateCallback;

  
  public MogoUpdateCallback getUpdateCallback(){
	  return mUpdateCallback;
  
  }
  public void onLoginCancel(String msg)
  {
    Log.e("MainActivity", "onLogincancel");
    UnityPlayer.UnitySendMessage(this.mGameobject, "OnCancel", msg);
  }

  public void onLoginFail(String msg)
  {
    Log.e("MainActivity", "onLoginFail");
    UnityPlayer.UnitySendMessage(this.mGameobject, "onError", msg);
  }

  public void onLoginDone(String msg)
  {
    Log.e("MainActivity", "onLoginDone");
    UnityPlayer.UnitySendMessage(this.mGameobject, "OnComplete", msg);
  }
  public void onLoginSPlat(String msg)
  {
    Log.e("MainActivity", "onLoginDone");
    UnityPlayer.UnitySendMessage(this.mGameobject, "onShowSPlat", msg);
  }
  public void OnChargeSPlat(String msg)
  {
    Log.e("MainActivity", "onLoginDone");
    UnityPlayer.UnitySendMessage(this.mGameobject, "OnChargeSPlat", msg);
  }
  
  public abstract void switchAccount();

  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    this.mContext = this;


    Log.e("MainActivity", "start init");
    Log.e("MainActivity", "init done");
  }


  public void init(String goName)
  {
    Log.e("MainActivity", "init");
    this.mGameobject = goName;
  }

  public void setUpdateCallBack(String name) {
    Log.e("MainActivity", "init");
    this.mUpdateCallback = new MogoUpdateCallback(name);
  }


  public abstract void showAssistant(String paramString);

  private static native boolean nativeScreenShot(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

  public abstract boolean isLoginned();

  public abstract void login();

  public void gotoNetworkSetting()
  {
    Log.d("MainActivity", "gotoNetworkSetting");
  }

  public void restartGame()
  {
    Log.d("MainActivity", "restartGame");
  }

  public abstract void charge(String paramString1, int paramInt, String paramString2);

  public abstract void createRoleLog(String paramString1, String paramString2);

  public abstract void roleLevelLog(String paramString1, String paramString2);

  public abstract void gotoGameCenter();

  public abstract void updateVersion();

  public void showLog(String log)
  {
    Log.d("MainActivity", log);
  }

  public abstract void clean();

  public abstract void sendLoginLog(String paramString);

  public abstract void smallActivate();

  public abstract void activityOpenLog();

  public abstract void activityBeforeLoginLog();

  public abstract void loginForm();

  public void InstallApk(final String fileName)
  {
   
  }

  protected void onStart()
  {
    Log.d("MainActivity", "onStart");
    super.onStart();
  }

  protected void onRestart()
  {
    Log.d("MainActivity", "onRestart");
    super.onRestart();
  }

  protected void onPause()
  {
    Log.d("MainActivity", "onPause");

    MogoNotificationManager.onSetupDone(this, "");
    super.onPause();
  }

  public void setupDailyNotification(String time, String type, String tag, String title, String titleBanner, String content)
  {
    Log.d("MainActivity", "setupDailyNotification:" + time);

    MogoNotificationManager.setupDailyNotification(this, time, type, tag, 
      title, titleBanner, content);
  }

  public void addNotification(long time) {
    MogoNotificationManager.addEnergyFullNotification(this, time);
  }

  public void onSetupNotificationDone(String data) {
    MogoNotificationManager.onSetupDone(this, data);
  }

  protected void onStop()
  {
    Log.d("MainActivity", "onStop");
    super.onStop();
  }

  public void onResume()
  {
    Log.d("MainActivity", "onResume");
    MogoNotificationManager.stopNotification(this);
    super.onResume();
  }

  protected void onDestroy()
  {
    Log.d("MainActivity", "onDestroy");
    super.onDestroy();
  }

  public void cancelRecord(int tag)
  {
    MogoNotificationManager.updateConfig(this, tag, 
      1);
  }

  public void addRecord(int tag) {
    MogoNotificationManager.updateConfig(this, tag, 
      0);
  }
}