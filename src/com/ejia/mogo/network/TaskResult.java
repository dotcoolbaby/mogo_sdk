package com.ejia.mogo.network;

/**
 * Task执行完毕后返回的结果
 *
 * @author Tsimle
 */
public class TaskResult {

    private int stateCode; // 状态码，一般为HTTP的响应码
    private String msg = "";
    private GenericTask task; // 任务对象本身
    private Object content; // 任务处理结果对象。也可以是错误消息
    private int code;

    public TaskResult() {
    }

    public TaskResult(int stateCode, GenericTask task, Object result) {
        super();
        this.stateCode = stateCode;
        this.task = task;
        this.content = result;
    }

    public GenericTask getTask() {
        return task;
    }

    public void setTask(GenericTask task) {
        this.task = task;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object retObj) {
        this.content = retObj;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
