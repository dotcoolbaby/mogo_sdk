package com.ejia.mogo.network;

import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.ejia.mogo.network.httpUtil.CodeConstant;
import com.ejia.mogo.network.httpUtil.CookiesManager;
import com.ejia.mogo.network.httpUtil.SafeHttpClient;
import com.ejia.mogo.network.parse.IParser;
import com.ejia.mogo.utils.LogUtil;
import com.google.common.collect.Maps;
/**
 * The Class RequestTask.
 */
public class RequestTask extends GenericTask {

    private static final String TAG = "RequestTask";

    private static final String KEY_JSESSIONID = "JSESSIONID";

    //post.setHeader("method", "ajax");
    private static final String HEADER_METHOD_KEY = "method";
    private static final String HEADER_METHOD_VALUE = "ajax";
    //post.setHeader("Accept-Language", "zh-cn,zh;q=0.5");
    private static final String HEADER_ACCEPT_LANGUAGE_KEY = "Accept-Language";
    private static final String HEADER_ACCEPT_LANGUAGE_VALUE = "zh-cn,zh;q=0.5";

    /**
     * 一般get请求.
     */
    public static final String HTTP_GET = "GET";

    /**
     * 一般post请求.
     */
    public static final String HTTP_POST = "POST";

    /**
     * post请求,传json参数.
     */
    public static final String HTTP_POST_JSON = "POST_JSON";

    public static final String PARAM_URL = "url";

    public static final String PARAM_HTTP_METHOD = "httpmethod";

    /**
     * 解析器.
     */
    private IParser mParser;

    private List<NameValuePair> mPostParams;

    private Map<String, String> mHeaders = Maps.newHashMap();

    private String mPostString;

    private Bitmap mImage;

    private String mImageName;

    private TaskParams mParams;

    private String mType;

    private String mUrl;

    /**
     * 相关对象的引用.
     */
    private Object mExtra;

    private boolean isNeedCookie = false;

    private boolean isNeedSaveCookieStore = false;

    /**
     * Instantiates a new request task.
     *
     * @param threadPriority the thread priority
     * @param parser         the parser
     */
    public RequestTask(final int threadPriority, IParser parser) {
        super(threadPriority);
        this.mParser = parser;
        initHeaders();
    }

    private void initHeaders() {
        mHeaders.put(HEADER_METHOD_KEY, HEADER_METHOD_VALUE);
        mHeaders.put(HEADER_ACCEPT_LANGUAGE_KEY, HEADER_ACCEPT_LANGUAGE_VALUE);
    }

    /**
     * Instantiates a new request task.
     *
     * @param parser the parser
     */
    public RequestTask(IParser parser) {
        super();
        this.mParser = parser;
        initHeaders();
    }

    /**
     * Instantiates a new request task.
     *
     * @param parser the parser
     * @param image  the image
     */
    public RequestTask(IParser parser, Bitmap image) {
        super();
        this.mParser = parser;
        this.mImage = image;
        initHeaders();
    }

    /**
     * Instantiates a new request task.
     *
     * @param parser     the parser
     * @param image      the image
     * @param bitmapName the bitmap name
     */
    public RequestTask(IParser parser, Bitmap image, String bitmapName) {
        super();
        this.mParser = parser;
        this.mImage = image;
        this.mImageName = bitmapName;
        initHeaders();
    }

    /**
     * Sets the post params.
     *
     * @param params the new post params
     */
    public void setPostParams(List<NameValuePair> params) {
        mPostParams = params;
    }

    /**
     * Gets the params.
     *
     * @return the params
     */
    public TaskParams getParams() {
        return mParams;
    }

    /**
     * Sets the post string.
     *
     * @param param the new post string
     */
    public void setPostString(String param) {
        this.mPostString = param;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return mType;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(String type) {
        this.mType = type;
    }

    /**
     * Gets the request url.
     *
     * @return the request url
     */
    public String getRequestUrl() {
        return mUrl;
    }

    /**
     * Gets the extra.
     *
     * @return the extra
     */
    public Object getExtra() {
        return mExtra;
    }

    /**
     * Sets the extra.
     *
     * @param relativeObj the new extra
     */
    public void setExtra(Object relativeObj) {
        this.mExtra = relativeObj;
    }

    /*
     * @see
     * com.sina.wemusic.network.GenericTask#onPostExecute(com.sina.wemusic.network
     * .TaskResult)
     */
    @Override
    protected void onPostExecute(TaskResult result) {
        // 当task没被取消时，调用父类回调taskFinished
        if (!isCancelled()) {
            super.onPostExecute(result);
        }
    }

    /**
     * Cancel.
     */
    public void cancel() {
        onCancelled();
    }

    public void addHeader(String key, String value) {
        if (key != null) {
            mHeaders.put(key, value);
        }
    }

    /**
     * @param params The parameters of the task.
     * @return
     */
    /*
     * @see com.sina.wemusic.network.AbsNormalAsyncTask#doInBackground(Params[])
     */
    @Override
    protected TaskResult doInBackground(TaskParams... params) {
        TaskResult result = new TaskResult(-1, this, null);
        mParams = params[0];
        if (mParams == null) {
            return result;
        }

        mUrl = mParams.getString(PARAM_URL);
        String fullUrl = addAuthInfo(mUrl);

        SafeHttpClient client = null;
        HttpResponse response = null;
        HttpEntity entity = null;
        try {
            client = SafeHttpClient.createHttpClient();

            CookieStore cookieStore = CookiesManager.getStance().getCookieStore();
            if (cookieStore != null && isNeedCookie) {
                client.setCookieStore(cookieStore);
            }

            if (HTTP_POST.equals(mParams.getString(PARAM_HTTP_METHOD))) {
                if (mImage != null) {
                    if (TextUtils.isEmpty(mImageName)) {
                        mImageName = "uploadfile";
                    }
                    response = HttpUtil.doFilePostRequest(client, fullUrl, mPostParams, mImage,
                            mImageName, mHeaders);
                } else {
                    response = HttpUtil.doPostRequest(client, fullUrl, mPostParams, mHeaders);
                }
            } else if (HTTP_POST_JSON.equals(mParams.getString(PARAM_HTTP_METHOD))) {
                response = HttpUtil.doPostRequest(client, fullUrl, mPostString, mHeaders);
            } else {
                response = HttpUtil.doGetRequest(client, fullUrl, mHeaders);
            }
            int stateCode = response.getStatusLine().getStatusCode();
            if (stateCode == HttpStatus.SC_OK || stateCode == HttpStatus.SC_PARTIAL_CONTENT) {
                result.setStateCode(HttpStatus.SC_OK);
                CookieStore store = client.getCookieStore();
                if (store != null) {
                    List<Cookie> cookies = store.getCookies();
                    LogUtil.e("mCookie：" + cookies.toString());
                    if (cookies != null && !cookies.isEmpty()&& isNeedSaveCookieStore) {
                        CookiesManager.getStance().saveCookieStore(store);
                    }
                }
                if (!this.isCancelled()) {
                    entity = response.getEntity();
                    String inputContent = EntityUtils.toString(entity, HTTP.UTF_8);
                    if (inputContent != null && mParser != null) {
                        Object obj = mParser.parse(inputContent);
                        result.setContent(obj);
                        result.setCode(mParser.getCode());
                    }
                }
            }
        } catch (Exception e) {
            LogUtil.e(TAG, e.toString());
        } finally {
            if (client != null) {
                client.getConnectionManager().shutdown();
            }
        }
        return result;
    }

    /**
     * Adds the auth info.
     *
     * @param url the url
     * @return the string
     */
    private String addAuthInfo(String url) {
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(CodeConstant.HOST_URL)) {
            if (url.contains("?")) {
                sb.append("&");
            } else {
                sb.append("?");
            }
            sb.append("client=android");

            // LoginInfo info = LoginManager.getStance().getLoginInfo();
            // if (info != null && info.getUserInfo() != null) {
            // sb.append("&userId=").append(info.getUID());
            // }
        }
        return sb.toString();
    }

    public void setNeedCookie(boolean isNeedCookie) {
        this.isNeedCookie = isNeedCookie;
    }

    public void setNeedSaveCookieStore(boolean isNeedSaveCookieStore) {
        this.isNeedSaveCookieStore = isNeedSaveCookieStore;
    }
}
