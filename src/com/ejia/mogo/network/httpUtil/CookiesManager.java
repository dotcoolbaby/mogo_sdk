package com.ejia.mogo.network.httpUtil;

import java.util.Date;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ejia.mogo.utils.LogUtil;
import com.ejia.mogo.utils.PreferenceUtil;

/**
 * cookies处理
 *
 * @author leewenbil
 */
public class CookiesManager {

    public static final String KEY_PRE_COOKIE_STORE = "key_pre_cookie_store";

    private static CookiesManager stance = new CookiesManager();

    private BasicCookieStore mCookieStore;

    public static CookiesManager getStance() {
        return stance;
    }

    private CookiesManager() {
    }

    public void initCookie() {
        String cookieStr = PreferenceUtil.getString(KEY_PRE_COOKIE_STORE, null);
      
        if (!TextUtils.isEmpty(cookieStr)) {
            try {
                JSONObject cookiesJObject = JSON.parseObject(cookieStr);
                JSONArray cookiesJArray = cookiesJObject.getJSONArray("cookies");
                if (cookiesJArray != null && cookiesJArray.size() > 0) {
                    int size = cookiesJArray.size();
                    BasicClientCookie[] basicClients = new BasicClientCookie[size];
                    BasicClientCookie tempCookie;
                    for (int i = 0; i < size; i++) {
                        JSONObject jsonObject = cookiesJArray.getJSONObject(i);
                        tempCookie = new BasicClientCookie(jsonObject.getString("name"),
                                jsonObject.getString("value"));
                        tempCookie.setPath(jsonObject.getString("path"));
                        tempCookie.setDomain(jsonObject.getString("domain"));
                        tempCookie.setSecure(jsonObject.getBoolean("secure"));
                        tempCookie.setVersion(jsonObject.getInteger("version"));

                        if (jsonObject.getBoolean("persistent")) {
                            tempCookie.setExpiryDate(new Date(jsonObject.getLongValue
                                    ("expiryDate")));
                        }

                        basicClients[i] = tempCookie;
                    }
                    mCookieStore = new BasicCookieStore();
                    mCookieStore.addCookies(basicClients);
                    LogUtil.e("RequestTask.mCookieStore:" + mCookieStore.toString());
                }

            } catch (Exception e) {
                LogUtil.e(e);
            }
        }
    }

    public void saveCookieStore(CookieStore cookieStore) {
        LogUtil.e("saveCookieStore-->cookieStore:" + cookieStore.toString());
        mCookieStore = (BasicCookieStore) cookieStore;
        if (cookieStore != null) {
            try {
                PreferenceUtil.saveString(KEY_PRE_COOKIE_STORE,
                        JSON.toJSONString(cookieStore));
            } catch (Exception e) {
                LogUtil.e(e);
            }
        } else {
            cleanCookieStore();
        }
    }

    public void cleanCookieStore() {
        mCookieStore = null;
        PreferenceUtil.saveString(KEY_PRE_COOKIE_STORE, null);
    }

    public BasicCookieStore getCookieStore() {
        return mCookieStore;
    }
}
