package com.ejia.mogo.network.httpUtil;

/**
 * @author leewenbil
 */
public class CodeConstant {

    public static final int CODE_SUCCESS = 200;
    public static final int CODE_FAIL = 400;

    public static final int ACCESSTOKEN_EXPIRED_CODE1 = 21327;
    public static final int ACCESSTOKEN_EXPIRED_CODE2 = 21332;

    /**
     * *********************************** SharedPreference *****************************************
     */

    public static final String PREFER_LOGIN_INFO = "login_info_prefer";

    /**
     * 登录数据key：用户id
     */
    public static final String PREFER_KEY_UID = "uid";

    /**
     * 登录数据key：访问令牌
     */
    public static final String PREFER_KEY_ACCESS_TOKEN = "access_token";

    /**
     * 登录数据key：微博头像
     */
    public static final String PREFER_KEY_USER_AVATAR = "avatar";

    /**
     * 登录数据key：访问令牌有效时间（秒 ）戳
     */
    public static final String PREFER_KEY_EXPIRES_IN = "expires_in";

    /**
     * 登录数据key：访问令牌失效时间（秒 ）
     */
    public static final String PREFER_KEY_EXPIRES_TIME = "expires_time";

    /**
     * 登录数据key：访问令牌失效时间（秒 ）
     */
    public static final String PREFER_KEY_USER_NICKNAME = "user_nickname";

    /**
     * 登录数据key：访问令牌失效时间（秒 ）
     */
    public static final String PREFER_KEY_LOGIN_TYPE = "login_typr";
    public static final String PREFER_KEY_USER_INGOT = "user_ingot";
    public static final String PREFER_KEY_USER_PAY = "user_pay";

    public static final String HEADER_KEY_REFER="Referer";
    public static final String HEADER_VALUE_REFER="http://www.ejiakt.com";
    
    public static final String HOST_URL = "http://www.52sourcecode.com";

}
