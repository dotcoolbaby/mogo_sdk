package com.ejia.mogo.network.parse;

import org.json.JSONException;

/**
 * 解析器接口
 */
public interface IParser {

    public Object parse(String jsonString) throws JSONException;

    public int getCode() ;
}
