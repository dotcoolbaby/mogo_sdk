package com.ejia.mogo.network.parse;


import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseParser implements IParser {
    public static final String TAG = "Parser";

    protected int code = 400;
    protected String msg = "";

    public int getCode() {
        return code;
    }

    protected void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    protected void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 返回的json是"status":{ "code":"000000", "msg":"成功" }, "data":{}的格式时
     * 使用该法解析出code，判断code为成功时，返回data的内容
     *
     * @param jsonString
     * @return
     * @throws JSONException
     */
    protected void parseDataContent(String jsonString) throws JSONException {
        JSONObject result = new JSONObject(jsonString);
        // JSONObject status = result.getJSONObject("status");
        if (result != null) {
            code = result.optInt("code", 400);
            msg = result.optString("msg", "");
        }
    }
}
