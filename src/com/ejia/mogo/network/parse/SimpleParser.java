package com.ejia.mogo.network.parse;

import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.ejia.mogo.network.httpUtil.CodeConstant;

public class SimpleParser implements IParser {
    public static final String TAG = "Parser";

    private Class<?> mClazz;

    protected int code = 400;
    protected String msg = "";
    protected String content;
    protected boolean isListData = false;

    public SimpleParser() {
        isListData = false;
    }

    public SimpleParser setListData(boolean isListData) {
        this.isListData = isListData;
        return this;
    }

    public SimpleParser(Class<?> clazz) {
        mClazz = clazz;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public Object parse(String jsonString) throws JSONException {
        JSONObject result = new JSONObject(jsonString);
        String content = result.optString("content", "");
        code = result.optInt("code", CodeConstant.CODE_FAIL);
        if (code != CodeConstant.CODE_FAIL) {
            String data = result.optString("data");
            if (mClazz == null || mClazz == String.class) {
                return data;
            } else {
                if (!isListData) {
                    return JSON.parseObject(data, mClazz);
                } else {
                    return JSON.parseArray(data, mClazz);
                }
            }
        } else {
            return content;
        }
    }
}
