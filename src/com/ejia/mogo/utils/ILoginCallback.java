package com.ejia.mogo.utils;

public abstract interface ILoginCallback
{
  public abstract void onLoginDone(String paramString);

  public abstract void onLoginFail(String paramString);

  public abstract void onLoginCancel(String paramString);
}