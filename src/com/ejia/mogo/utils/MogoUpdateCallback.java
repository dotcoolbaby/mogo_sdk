package com.ejia.mogo.utils;

import com.unity3d.player.UnityPlayer;

public class MogoUpdateCallback
implements IUpdateCallBack
{
private String mGoName;

public MogoUpdateCallback(String goName)
{
  this.mGoName = goName;
}

public void onNotNewVersion()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnNotNewVersion", "");
}

public void onNotSDCard()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnNotSDCard", "");
}

public void onCancelNormalUpdate()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnCancelNormalUpdate", "");
}

public void onCheckVersionFailure()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnCheckVersionFailure", "");
}

public void onForceUpdateLoading()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnForceUpdateLoading", "");
}

public void onNormalUpdateLoading()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnNormalUpdateLoading", "");
}

public void onNetWorkError()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnNetWorkError", "");
}

public void onUpdateException()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnUpdateException", "");
}

public void onCancelForceUpdate()
{
  UnityPlayer.UnitySendMessage(this.mGoName, "OnCancelForceUpdate", "");
}
}