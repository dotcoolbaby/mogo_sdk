package com.ejia.mogo.utils;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.widget.Toast;

import com.ejia.mogo.MainActivity;

/**
 * @author Lee Wenbil
 * @Description: 存储卡管理类
 */
public class StorageUtil {
    // private static final String TAG = "StorageUtil";

    public static final String EXTERNAL_STORAGE = Environment.getExternalStorageDirectory()
            .toString();

    /**
     * SDCard主目录.
     */
    private static final String DIR_HOME = EXTERNAL_STORAGE + "/ejiakt/video";

    private static String APK_DIR = DIR_HOME + "/apk";
    private static String IMAGE_DIR = DIR_HOME + "/image";
    private static String BOOK_DIR = DIR_HOME + "/book";
    private static String MARK_DIR = DIR_HOME + "/mark";
    private static String LOG_DIR = DIR_HOME + "/log";

    public static final int DIR_TYPE_BOOK = 21;
    public static final int DIR_TYPE_IMAGE = 23;
    public static final int DIR_TYPE_APK = 25;
    public static final int DIR_TYPE_MARK = 24;
    public static final int DIR_TYPE_LOG = 26;

    public static final String TEMP_SUFFIX = ".tmp";

    /**
     * 该文件用来在图库中屏蔽本应用的图片.
     */
    private static final String DIR_NO_MEDIA_FILE = DIR_HOME + "/.nomedia";

    private static final Context mContext = MainActivity.gContext;

    private StorageUtil() {
    }

    public static boolean hasExternalStorage() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static String getDirByType(int type) {
        String dir = "/";
        String filePath = "";
        switch (type) {
            case DIR_TYPE_BOOK: {
                filePath = BOOK_DIR;
                break;
            }
            case DIR_TYPE_IMAGE: {
                filePath = IMAGE_DIR;
                break;
            }
            case DIR_TYPE_APK: {
                filePath = APK_DIR;
                break;
            }
            case DIR_TYPE_MARK: {
                filePath = MARK_DIR;
                break;
            }
            case DIR_TYPE_LOG: {
                filePath = LOG_DIR;
                break;
            }
        }

        File file = new File(filePath);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
        if (file.exists()) {
            if (file.isDirectory()) {
                dir = file.getPath();
            }
        } else {
            // 文件没创建成功，可能是sd卡不存在，但是还是把路径返回
            dir = filePath;
        }
        return dir;
    }

    /**
     * 是否插入sdcard
     *
     * @return
     */
    public static boolean isSDCardExist() {
        boolean sdCardExist = false;
        sdCardExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        return sdCardExist;
    }

    /**
     * 判断存储空间是否足够
     *
     * @param needSize
     * @return
     */
    public static boolean checkExternalSpace(float needSize) {
        boolean flag = false;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File sdcardDir = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(sdcardDir.getPath());
            long blockSize = sf.getBlockSize();
            long availCount = sf.getAvailableBlocks();
            long restSize = availCount * blockSize;
            if (restSize > needSize) {
                flag = true;
            } else {
                Toast.makeText(mContext, "存储卡空间不足", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mContext, "没有存储卡", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    public static boolean changeFileSuffix(String oldName, String newName) {
        File tmpFile = new File(oldName);
        boolean flag = tmpFile.renameTo(new File(newName));

        if (tmpFile.exists())
            tmpFile.delete();

        return flag;
    }

    /**
     * 避免图片放入到图库中（屏蔽其他软件扫描）.
     */
    public static void hideMediaFile() {
        File file = new File(DIR_NO_MEDIA_FILE);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
        } catch (IOException e) {
            LogUtil.e(e);
        }
    }
}
