package com.ejia.mogo.utils;

import android.widget.Toast;

import com.ejia.mogo.MainActivity;

public class VersionUpdateListener
{

  public void onNotNewVersion()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "没有发现新版本", 
      1).show();
   
  }

  public void onNotSDCard()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "没有检查到SD卡", 
      1).show();
  }

  public void onCancelForceUpdate()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "取消强制更新", 1)
      .show();
  }

  public void onCancelNormalUpdate()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "取消一般更新", 1)
      .show();
  }

  public void onCheckVersionFailure()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "版本检查失败", 1)
      .show();
  }

  public void onForceUpdateLoading()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "强制更新中", 1)
      .show();
  }

  public void onNormalUpdateLoading()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "一般更新中", 1)
      .show();
  }

  public void onNetWorkError()
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "网络异常", 1)
      .show();
  }

  public void onSsjjsyException(Exception e)
  {
    Toast.makeText(MainActivity.gContext.getApplicationContext(), "异常", 1)
      .show();

  }
}
