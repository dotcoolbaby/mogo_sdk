package com.ejia.mogo.utils;

public abstract interface IUpdateCallBack
{
  public abstract void onNotNewVersion();

  public abstract void onNotSDCard();

  public abstract void onCancelNormalUpdate();

  public abstract void onCheckVersionFailure();

  public abstract void onForceUpdateLoading();

  public abstract void onNormalUpdateLoading();

  public abstract void onNetWorkError();

  public abstract void onUpdateException();

  public abstract void onCancelForceUpdate();
}