package com.ejia.mogo.utils;

import android.os.Bundle;

public class Config {
	  public static String username;
	  public static String timestamp;
	  public static String signStr;
	  public static String suid;
	  public static String targetServerId;
	  public static String comeFrom;
	  public static String token;
	  public static String sessionId;
	  public static boolean isLoginDone = false;
	  public static void CleanConfig(){
		  username = "";
		  timestamp = "";
		  signStr = "";
		  suid = "";
		  targetServerId = "";
		  comeFrom = "";
		  token = "";
		  isLoginDone = false;
	  }
	  public static void SetupConfig(Bundle values)
	  {
	    username = values.getString("username");

	    timestamp = values.getString("timestamp");

	    signStr = values.getString("signStr");

	    suid = values.getString("suid");

	    targetServerId = values.getString("targetServerId");

	    comeFrom = values.getString("comeFrom");

	    token = values.getString("verifyToken");
	    isLoginDone = true;
	  }
}
