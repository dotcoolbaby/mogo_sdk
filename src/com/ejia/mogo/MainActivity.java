package com.ejia.mogo;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.ejia.mogo.utils.Config;


 public class MainActivity extends MogoMainActivityParent
{
  public static final String DEBUG_TAB = "MainActivity";

  public static Context gContext;
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    gContext = this;
  }

  protected void sdkInit()
  {
    Log.e("MainActivity","sdkInit");
    Config.CleanConfig();
    onLoginSPlat("switchAccount");
  }

  public void showAssistant(final String serverId)
  {
	  Log.e("MainActivity", "showAssistant:" + serverId);
  }

  private static native boolean nativeScreenShot(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4);

 

  public void switchAccount()
  {
	  Config.CleanConfig();
	  onLoginSPlat("switchAccount");
  }

public void login()
  {
    Log.e("MainActivity", "login");
    onLoginSPlat("login");
   
  }

  public void charge(String serverId, final int amount, final String callbackMsg)
  {
    Log.e("MainActivity", "charge:" + amount);
    super.OnChargeSPlat(serverId);
   
  }

  public void gotoGameCenter()
  {
    Log.e("MainActivity", "gotoGameCenter");
   
  }

  public void updateVersion()
  {
	  Log.e("MainActivity", "updateVersion");
  }

  public void clean()
  {
    Log.e("MainActivity", "clean");

  }

  public void sendLoginLog(String serverId)
  {
    Log.e("MainActivity", "sendLoginLog:" + serverId);
 

    Log.e("MainActivity", "going to restartGame");

    Log.e("MainActivity", "restartGame done");
  }

  public void smallActivate()
  {
  }

  public void activityOpenLog()
  {
    Log.e("MainActivity", "activityOpenLog");
  }

  public void activityBeforeLoginLog()
  {
    Log.e("MainActivity", "activityBeforeLoginLog:");
  }

  public void loginForm()
  {
    Log.e("MainActivity", "loginForm");
   
  }

  protected void onStart()
  {
    Log.e("MainActivity", "onStart");
    super.onStart();
  }

  protected void onRestart()
  {
    Log.e("MainActivity", "onRestart");
    super.onRestart();
  }

  protected void onPause()
  {
    Log.e("MainActivity", "onPause");
    super.onPause();
  }

  protected void onStop()
  {
    Log.e("MainActivity", "onStop");
    super.onStop();
  }

  public void onResume()
  {
    Log.d("MainActivity", "onResume");
    super.onResume();
  }

  protected void onDestroy()
  {
    Log.e("MainActivity", "onDestroy");
    super.onDestroy();
  }

  public void createRoleLog(String roleName, String serverId)
  {
  }

  public void roleLevelLog(String roleLevel, String serverId)
  {
  }
  public void enterGameLog(String signature)
  {
	  Log.e("MainActivity", "enterGameLog");  
  }

  
@Override
public boolean isLoginned() {
	// TODO Auto-generated method stub
	return false;
}
}